module.exports = function(eleventyConfig) {
  eleventyConfig.addPassthroughCopy("./src/assets");
  eleventyConfig.addPassthroughCopy("src/**/*.jpg");
  eleventyConfig.addPassthroughCopy("src/**/*.png");
  eleventyConfig.addPassthroughCopy("src/**/*.svg");
  eleventyConfig.addWatchTarget("./src/assets/");
  // markdown plugins:
  // https://www.11ty.dev/docs/languages/markdown/#optional%3A-set-your-own-library-instance
  let markdownIt = require("markdown-it");
  let markdownItDiv = require('markdown-it-div');
  let markdownItAttrs = require('markdown-it-attrs');
  let markdownSpans = require('markdown-it-bracketed-spans');
  let options = {
    html: true,
    breaks: true,
    linkify: true
  };
  let markdownLib = markdownIt(options).use(markdownItDiv).use(markdownItAttrs).use(markdownSpans);
  eleventyConfig.setLibrary("md", markdownLib);
  // Return your Object options:
  return {
    dir: {
      input: "src",
      data: "data",
      includes: "layouts",
      output: "website_product"
    }
  };
};
