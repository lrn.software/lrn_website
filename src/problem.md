---
id: 'problem'
tags: home
date: 2000-01-02
---
### Popular education options are often *terrible*

*Academia* and *Industry* don't talk. There are millions of search results on google for the [gap between academia and industry](https://www.google.com/search?q=gap+between+academia+and+industry){target="_blank"}. When it comes to the software industry, educations suffers two severe problems:

- **Obfuscation**: *making simple things difficult and complex*. University curriculums are sometimes so obfuscated that students get confused about the *usefulness* of the `boolean` data type or the *purpose* of the `print()` statement. This happens, it's terrible.
- **Plastic toy hammers**: these are not used in trade school, but we use them in computer programming education with web-based simulators and `hello_world()` scripts.

No learning programs use a professional toolkit as the learning environment &mdash; until now…{style="text-align:center;"}
