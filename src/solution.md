---
id: 'solution'
tags: home
date: 2000-01-03
---
## THE SOLUTION

**real-world code [∙]{.divide} real engineering tools [∙]{.divide} efficient explanations** {style="text-align:center;text-transform:uppercase;font-size:.8em;}

**Automate the manual processes of learning to code**
Faster and easier learning by allowing the student to focus simply on the code itself {style="text-align:center;"}

**Real tools, real code &mdash; no plastic toy hammers!**
To create more effective and empowering learning experiences. {style="text-align:center;"}

Automating the learning session frees the student from having to know how the tools work; they're there "under the hood" for the student to learn at their own pace.

#### Computer programming education unlike ever before
`lrn` enables us to do things that no other curriculums in the world do:
  - demonstrate language constructs and nuances by showing live scripts from different programming languages running side-by-side
  - compare software design patterns by loading source code from different projects side-by-side
  - run an already complete database-driven app to allow the student to interact with it and observe how it works
  - automate steps like: run the script, reload the web browser, etc

With these tools that `lrn` brings together, including `linux`, `tmux`, `docker` and more, we can develop a whole set of curriculums more excellent than anyone's yet to imagine.

:::#freedom
### Accessibility, opportunity and justice for all
`lrn` and the entire toolkit are free, the curriculums and lessons are free, it works offline with no internet connection, and it runs on old computers that you can salvage for free!
:::
