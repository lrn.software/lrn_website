---
title: 'Contact'
description: Contact Ahelio Digitalis.
date: 2020-02-02T00:00:00Z
---

Contact Alec Hill at:\
802-279-8800

<form name="contact" method="POST" netlify-honeypot="bot-field" data-netlify="true">
  <p class="hidden">
    <label>Don’t fill this out if you’re human: <input name="bot-field" /></label>
  </p>
  <p>
    <label>Your Name: <input type="text" name="name" /></label>   
  </p>
  <p>
    <label>Your Email: <input type="email" name="email" /></label>
  </p>
  <p>
    <label>Message: <textarea name="message"></textarea></label>
  </p>
  <p>
    <button type="submit">Send</button>
  </p>
</form>

Lab Space @Generator in Burlington, VT\
40 Sears Lane

Snail mail:\
PO Box 4556\
Burlington, VT 05406
