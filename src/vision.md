## Vision for `lrn` software and curriculums

We can 

Revolutionize computer programming education - revolutionize the place of software in society.
The potential to revolutionize how computer programming is taught gives us the potential to revolutionize the place and effect of software in the world.

We can move away from monopolized tech conglomerates by establishing a public commons that enables opportunity, freedom and liberty.

We can create this public commons by training an entire generation in open source software development.
