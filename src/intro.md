---
id: 'intro'
tags: home
date: 2000-01-01
---

## Computer programming is not difficult, *learning* computer programming is what's difficult.

The `lrn` software facilitates lessons in computer programming.
{.center}

It runs inside a Linux terminal to bring students up to speed with real-world coding faster than ever before.
{.center}

<script id="asciicast-456537" src="https://asciinema.org/a/456151.js" async></script>

*Expand the demo to fullscreen mode* &uarr; {style="text-align:right;position:relative;top:-2.2em;background:white;font-size:.8em;"}

