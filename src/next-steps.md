---
id: 'next-steps'
tags: home
date: 2000-01-04
---

### Start Coding
Test-drive the <code>lrn</code> software on your Linux system. {style="text-align:center;"}

The first lesson is on [parsing text files in Python](https://gitlab.com/lrn.software/parsing_text_files__python){target="_blank"}.

Get started by using [the Debian installer](https://gitlab.com/lrn.software/lrn_installer){target="_blank"} to install `lrn` and download the lesson.  It's as simple as:

```sh
git clone https://gitlab.com/lrn.software/lrn_installer.git
bash lrn_installer/install.sh
lrn
```

---

### Questions? Comments?

<!-- netlify webform, no js needed in template, just configure notification in netlify ui -->
<form name="contact" method="POST" netlify-honeypot="bot-field" data-netlify="true">
  <p class="hidden">
    <label>Don’t fill this out if you’re human: <input name="bot-field" /></label>
  </p>
  <p>
    <label>Your Name:</label><input type="text" name="name" class="border" />
  </p>
  <p>
    <label>Your Email:</label><input type="email" name="email" class="border" />
  </p>
  <p id="interests">
    <input type="checkbox" id="interest1" name="interest" value="notify-me">
    <label for="interest1">Notify me when more lessons are available</label><br>
    <input type="checkbox" id="interest2" name="interest" value="contribute">
    <label for="interest2">I want to help develop lessons</label><br>
    <input type="checkbox" id="interest3" name="interest" value="funding">
    <label for="interest3">I want to help with fundraising</label>
  </p>
  <p>
    <label>Message:</label><textarea name="message" class="border"></textarea>
  </p>
  <p>
    <button type="submit">Send</button>
  </p>
</form>
